001. Runtime function [c#]
-------------------------------------------------------------------------------
EN: Measures the time of the function. The implementation using a timer.

RU: Измеряет время выполнения функции. Реализация при помощи таймера.



002. Convert to binary number [c#]
-------------------------------------------------------------------------------
EN: Convert a decimal number to binary number.

RU: Конвертирует десятичное число в двоичное.



003. Colored letters [c#]
-------------------------------------------------------------------------------
EN: Implementation colored letters and a background in console.

RU: Реализация цветных букв и фона в консоли.



004. Stack [c#]
-------------------------------------------------------------------------------
EN: Implementation of the stack which stores a different number of distinct
    values.

RU: Реализация стека хранящего различное количество различных значений.



005. Convert number to eng and a rus words [c#]
-------------------------------------------------------------------------------
EN: Convert number to words in russian and a english languages.

RU: Конвертирует число в слова на русском и английском языках.



006. Inheritance demo [c#]
-------------------------------------------------------------------------------
EN: Demonstration of inheritance in C#.

RU: Демонстрация наследования в C#.



007. Html parser [c#]
-------------------------------------------------------------------------------
EN: Html parser. Reading URL addresses of the D://Desktop/ImhoWork/in.txt and
	writes the source code HTML pages on the D://Desktop/ImhoWork/out.txt

RU: Html-парсер. Считывает URL адреса из D://Desktop/ImhoWork/in.txt и
	записывает исходные коды соответствующих web-страниц в D://Desktop/ImhoWork/
	out.txt



008. Sorting [c#]
-------------------------------------------------------------------------------
EN: Compare of the different sorting algorithms.

RU: Сравнение различных алгоритомов сортировки.
